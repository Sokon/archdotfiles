# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' max-errors 3
zstyle :compinstall filename '/home/pvinc/.zshrc'
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BNo matches for: %d%b'

autoload -Uz compinit
compinit

# export PS1="%F{red}<%F{yellow}%?%F{red}>[%F{yellow}%T %F{green}%2~%F{red}]%f%(#.#.$) "

source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(status time dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(history disk_usage ip)

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=50
SAVEHIST=100
bindkey -v
# End of lines configured by zsh-newuser-install
#
alias ls="ls --color=auto"
eval "$(thefuck --alias)"
