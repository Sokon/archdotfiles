set nu
syntax on

call plug#begin('$HOME/.config/nvim/plugged')
	Plug 'w0rp/ale'
	Plug 'vim-airline/vim-airline'
	Plug 'rakr/vim-one'
call plug#end()

" Theming section
set termguicolors
colorscheme one
set background=dark
let g:airline_theme = "one"

let g:airline_powerline_fonts = 1
set t_Co=256

let g:airline#extensions#tabline#enabled = 1

" JS linting section
let g:jsx_ext_required = 0

let g:ale_linters = {
\ 'javascript': ['eslint'],
\ }

let g:ale_fixers = {
\ 'javascript': ['prettier', 'eslint'],
\ 'css': ['prettier'],
\ }

let g:ale_sign_error = '>>'
let g:ale_sign_warning = '!>'
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
highlight ALEErrorSign ctermbg=red ctermfg=white
highlight ALEWarningSign ctermbg=yellow ctermfg=white

let g:ale_linters_explicit = 1
let g:ale_fix_on_save = 1
let g:ale_lint_on_save = 1
