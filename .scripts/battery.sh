#!/bin/bash

bat=`acpi -b | awk '{ print $4 }' | cut -b1-4 | tr -d ',%'`
charge=`acpi -b | awk '{ print $3 }' | tr -d ','`

if [ $bat -ge 50 ]; then
	echo -n '<span foreground="#00FF00">'
	echo -n "$bat% $charge"
	echo '</span>'
elif [ $bat -gt 20 ] && [ $bat -lt 50 ]; then
	echo -n '<span foreground="#FFFF00">'
	echo -n "$bat% $charge"
	echo '</span>'
elif [ $bat -le 20 ]; then
	echo -n '<span foreground="#FF0000">'
	echo -n "$bat% $charge"
	echo '</span>'
fi
