#!/bin/bash

name=`iw dev wlp3s0 link | awk '/^\tSSID/ { print $2 }'`
power=`iw dev wlp3s0 link | awk '/^\tsignal/ { print $2 }'`

echo -n "$name "
if [ $power -ge -50 ]; then
	echo -n '<span foreground="#00FF00">'
	echo -n "$power dBm"
	echo '</span>'
elif [ $power -lt -50 ] && [ $power -ge -67 ]; then
	echo -n '<span foreground="#FFFF00">'
	echo -n "$power dBm"
	echo '</span>'
elif [ $power -lt -67 ] && [ $power -ge -70 ]; then
	echo -n '<span foreground="#FFA000">'
	echo -n "$power dBm"
	echo '</span>'
elif [ $power -lt -70 ]; then
	echo -n '<span foreground="#FF0000">'
	echo -n "$power dBm"
	echo '</span>'
fi
